//! Day 3: Perfectly Spherical Houses in a Vacuum

use std::collections::HashSet;

type Positions = HashSet<(i32, i32)>;

fn step(pos: &mut (i32, i32), visited: &mut Positions, dir: char) {
    match dir {
        '^' => {
            pos.1 += 1;
            visited.insert(*pos);
        }
        'v' => {
            pos.1 -= 1;
            visited.insert(*pos);
        }
        '>' => {
            pos.0 += 1;
            visited.insert(*pos);
        }
        '<' => {
            pos.0 -= 1;
            visited.insert(*pos);
        }
        _ => {}
    };
}

#[aoc(day3, part1)]
fn solve_part1(data: &str) -> u32 {
    let mut pos = (0, 0);
    let mut visited = Positions::new();
    visited.insert(pos);
    for dir in data.chars() {
        step(&mut pos, &mut visited, dir);
    }
    visited.len() as u32
}

#[aoc(day3, part2)]
fn solve_part2(data: &str) -> u32 {
    let mut pos_santa = (0, 0);
    let mut pos_robot = (0, 0);
    let mut visited = Positions::new();
    visited.insert((0, 0));
    for dirs in data.chars().collect::<Vec<char>>().chunks(2) {
        let dir_santa = dirs[0];
        let dir_robot = dirs[1];
        step(&mut pos_santa, &mut visited, dir_santa);
        step(&mut pos_robot, &mut visited, dir_robot);
    }
    visited.len() as u32
}
