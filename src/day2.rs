//! Day 2: I Was Told There Would Be No Math

type Gift = (u32, u32, u32);

fn box_area(w: u32, l: u32, h: u32) -> u32 {
    2 * (l * w + w * h + h * l)
}

fn box_volume(w: u32, l: u32, h: u32) -> u32 {
    w * l * h
}

fn smallest_side_area(w: u32, l: u32, h: u32) -> u32 {
    *vec![l * w, w * h, h * l].iter().min().unwrap()
}

fn smallest_perimeter(w: u32, l: u32, h: u32) -> u32 {
    let vals = vec![2 * w, 2 * l, 2 * h];
    let max_val = *vals.iter().max().unwrap();
    vals.iter().sum::<u32>() - max_val
}

#[aoc_generator(day2)]
pub fn parse_gifts(input: &str) -> Vec<Gift> {
    input
        .lines()
        .map(|l| {
            let mut gift = l.split('x').map(|d| d.parse().unwrap());
            (
                gift.next().unwrap(),
                gift.next().unwrap(),
                gift.next().unwrap(),
            )
        })
        .collect()
}

#[aoc(day2, part1)]
pub fn solve_part1(gifts: &[Gift]) -> u32 {
    gifts
        .iter()
        .map(|&(l, w, h)| box_area(w, l, h) + smallest_side_area(w, l, h))
        .sum()
}

#[aoc(day2, part2)]
pub fn solve_part2(gifts: &[Gift]) -> u32 {
    gifts
        .iter()
        .map(|&(l, w, h)| box_volume(w, l, h) + smallest_perimeter(w, l, h))
        .sum()
}
