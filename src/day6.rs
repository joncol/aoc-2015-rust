use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::digit1,
    combinator::{map, map_res, recognize},
    sequence::{pair, separated_pair},
    IResult,
};

const GRID_WIDTH: u32 = 1000;
const GRID_HEIGHT: u32 = 1000;

type Pos = (u32, u32);
type Rect = (Pos, Pos);

#[derive(Debug)]
pub enum Command {
    TurnOn,
    TurnOff,
    Toggle,
}

#[derive(Debug)]
pub struct Instruction {
    command: Command,
    rect: Rect,
}

mod parser {
    use super::*;

    /// Parses an unsigned 32-bit integer.
    fn uint32(input: &str) -> IResult<&str, u32> {
        map_res(recognize(digit1), str::parse)(input)
    }

    fn pos(input: &str) -> IResult<&str, Pos> {
        separated_pair(uint32, tag(","), uint32)(input)
    }

    fn rect(input: &str) -> IResult<&str, (Pos, Pos)> {
        separated_pair(pos, tag(" through "), pos)(input)
    }

    fn command(input: &str) -> IResult<&str, Command> {
        alt((
            map(tag("turn on "), |_| Command::TurnOn),
            map(tag("turn off "), |_| Command::TurnOff),
            map(tag("toggle "), |_| Command::Toggle),
        ))(input)
    }

    pub fn instruction(input: &str) -> IResult<&str, Instruction> {
        map(pair(command, rect), |(command, rect)| Instruction {
            command,
            rect,
        })(input)
    }
}

fn execute_instruction1(
    grid: &mut Vec<u8>,
    Instruction { command, rect }: &Instruction,
) {
    for y in rect.0.1..=rect.1.1 {
        for x in rect.0.0..=rect.1.0 {
            match command {
                Command::TurnOn => grid[(y * GRID_WIDTH + x) as usize] = 1,
                Command::TurnOff => grid[(y * GRID_WIDTH + x) as usize] = 0,
                Command::Toggle => grid[(y * GRID_WIDTH + x) as usize] =
                    1 - grid[(y * GRID_WIDTH + x) as usize],
            }
        }
    }
}

fn execute_instruction2(
    grid: &mut Vec<u8>,
    Instruction { command, rect }: &Instruction,
) {
    for y in rect.0.1..=rect.1.1 {
        for x in rect.0.0..=rect.1.0 {
            match command {
                Command::TurnOn => grid[(y * GRID_WIDTH + x) as usize] += 1,
                Command::TurnOff => {
                    if grid[(y * GRID_WIDTH + x) as usize] > 0 {
                        grid[(y * GRID_WIDTH + x) as usize] -= 1;
                    }
                },
                Command::Toggle => grid[(y * GRID_WIDTH + x) as usize] += 2,
            }
        }
    }
}

#[aoc_generator(day6)]
pub fn parse_instructions(input: &str) -> Vec<Instruction> {
    input.lines().map(|l| { parser::instruction(&l).unwrap().1 }).collect()
}

#[aoc(day6, part1)]
pub fn solve_part1(input: &[Instruction]) -> u32 {
    let mut grid: Vec<u8> = vec![0; (GRID_WIDTH * GRID_HEIGHT) as usize];
    for instr in input {
        execute_instruction1(&mut grid, &instr);
    }
    grid.iter().filter(|p| **p == 1).count() as u32
}

#[aoc(day6, part2)]
pub fn solve_part2(input: &[Instruction]) -> u32 {
    let mut grid: Vec<u8> = vec![0; (GRID_WIDTH * GRID_HEIGHT) as usize];
    for instr in input {
        execute_instruction2(&mut grid, &instr);
    }
    grid.iter().fold(0, |sum, x| sum + *x as u32)
}
