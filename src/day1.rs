//! Day 1: Not Quite Lisp

fn step(floor: i32, c: char) -> i32 {
    match c {
        '(' => floor + 1,
        ')' => floor - 1,
        _ => floor,
    }
}

#[aoc(day1, part1)]
pub fn solve_part1(input: &str) -> i32 {
    input.chars().fold(0, step)
}

#[aoc(day1, part2)]
pub fn solve_part2(input: &str) -> i32 {
    let result: Result<(i32, i32), i32> =
        input.chars().try_fold((0, 0), |(floor, i), c| {
            if floor == -1 {
                Err(i)
            } else {
                Ok((step(floor, c), i + 1))
            }
        });
    result.unwrap_err()
}
