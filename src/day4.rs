//! Day 4: The Ideal Stocking Stuffer

use crypto::digest::Digest;

fn find_digest_prefix_index(puzzle_input: &str, prefix: &str) -> u32 {
    let mut i: u32 = 0;
    let mut md5 = crypto::md5::Md5::new();
    loop {
        md5.input_str(&format!("{}{}", puzzle_input, i));
        let digest = md5.result_str();

        if digest.starts_with(prefix) {
            return i;
        }

        md5.reset();
        i += 1;
    }
}

#[aoc(day4, part1)]
pub fn solve_part1(input: &str) -> u32 {
    find_digest_prefix_index(input, "00000")
}

#[aoc(day4, part2)]
/// Note that this requires `--release` to finish in a reasonable time.
pub fn solve_part2(input: &str) -> u32 {
    find_digest_prefix_index(input, "000000")
}
