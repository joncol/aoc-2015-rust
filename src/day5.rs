use fancy_regex::Regex;

fn is_vowel(c: &char) -> bool {
    "aeiou".chars().any(|x| x == *c)
}

fn vowel_count(s: &str) -> u32 {
    s.chars().filter(|c| is_vowel(&c)).count() as u32
}

#[aoc(day5, part1)]
pub fn solve_part1(input: &str) -> u32 {
    let re = Regex::new(r"(.)\1").unwrap();
    input
        .lines()
        .filter(|l| {
            vowel_count(l) >= 3
                && re.is_match(l).unwrap()
                && !["ab", "cd", "pq", "xy"].iter().any(|w| l.contains(w))
        })
        .count() as u32
}

#[aoc(day5, part2)]
pub fn solve_part2(input: &str) -> u32 {
    let re1 = Regex::new(r"(..).*\1").unwrap();
    let re2 = Regex::new(r"(.).\1").unwrap();
    input
        .lines()
        .filter(|l| re1.is_match(l).unwrap() && re2.is_match(l).unwrap())
        .count() as u32
}
